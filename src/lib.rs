use std::borrow::{Borrow, BorrowMut};
use std::error::Error;
use std::fmt;
use std::fmt::Debug;
use std::mem;
use std::ops::Drop;
use std::ops::{Deref, DerefMut};
use std::panic::PanicInfo;
use std::rc::{Rc, Weak};
use std::thread::panicking;

#[derive(Debug)]
pub enum ErrorBBST {
    Unfound,
    CantRemoveroot,
}

#[derive(Clone)]
pub struct BalancedBinarySearchNode<T> {
    key: i32,
    data: Option<Box<T>>,
    left: Option<Box<BalancedBinarySearchNode<T>>>,
    right: Option<Box<BalancedBinarySearchNode<T>>>,
    // parent: Option<Weak<RefCell<BalancedBinarySearchNode<T>>>>,
}

impl<T> fmt::Display for BalancedBinarySearchNode<T>
    where
        T: Clone,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // let p = match &self.parent{
        //     Some(node) => Rc::clone(&node.upgrade().unwrap()).deref().borrow().key.to_string(),
        //     None => "None".to_string(),
        // };
        let r = match &self.right {
            Some(node) => node.deref().borrow().key.to_string(),
            None => "None".to_string(),
        };
        let l = match &self.left {
            Some(node) => node.deref().borrow().key.to_string(),
            None => "None".to_string(),
        };

        write!(f, "(key: {}, p: {}, l: {}, r: {})", self.key, '0', l, r)
    }
}

impl<T> BalancedBinarySearchNode<T>
    where
        T: Clone + Copy,
{

    // pub fn max(this: BalancedBinarySearchNode<T>) -> (i32, Option<Box<T>>){
    //     match this.right{
    //         Some(right) => {BalancedBinarySearchNode::max(*right)},
    //         None => {return (this.key, this.data)}
    //     }
    // }
    //
    // pub fn min(this: BalancedBinarySearchNode<T>) -> (i32, Option<Box<T>>){
    //     match this.left{
    //         Some(left) => {BalancedBinarySearchNode::min(*left)},
    //         None => {return (this.key, this.data)}
    //     }
    // }

        pub fn exists(&self, value: i32) -> bool {
        //println! {"{}  {} {}", self.key, value, self};
        if self.key == value {
            true
        } else {
            if value < self.key {
                match &self.left {
                    Some(node) => node.deref().borrow().exists(value),
                    None => false,
                }
            } else {
                match &self.right {
                    Some(node) => node.deref().borrow().exists(value),
                    None => false,
                }
            }
        }
    }

    // pub fn min(&self) -> (i32, Option<Box<T>>){
    //     match &self.left{
    //         Some(left) => {left.deref().borrow().min()},
    //         None => {return (self.key, Some(self.data))}
    //     }
    // }

        pub fn max(&self) -> (i32, Option<T>){
            match &self.right{
            Some(right) => {right.deref().borrow().max()},
            None => {
                if let Some(t_copy ) = &self.data{
                    return (self.key, Some(**t_copy))
                }
                else{
                    return (self.key, None)}
                }

        }
    }

    pub fn min(&self) -> (i32, Option<T>){
         match &self.left{
            Some(left) => {left.deref().borrow().max()},
            None => {
                if let Some(t_copy ) = &self.data{
                    return (self.key, Some(**t_copy))
                }
                else{
                    return (self.key, None)}
                }

        }
    }

    pub fn height(&self) -> i32 {
        let right_value: i32;
        let left_value: i32;
        match &self.right {
            Some(node) => right_value = node.deref().borrow().height(),
            None => right_value = 0,
        }

        match &self.left {
            Some(node) => left_value = node.deref().borrow().height(),
            None => left_value = 0,
        }

        if right_value > left_value {
            right_value + 1
        } else {
            left_value + 1
        }
    }

    pub fn new(value: i32, data: Option<Box<T>>) -> Self {
        BalancedBinarySearchNode {
            right: None,
            left: None,
            key: value,
            data: data,
            //     parent: None,
        }
    }

    pub fn insert(&mut self, value: i32, data: Option<Box<T>>) -> Result<(), ErrorBBST> {
        //println! {"{}  {} ", self, value};
        if value >= self.key {
            match self.right {
                Some(ref mut right) => BalancedBinarySearchNode::insert(right, value, data),
                None => {
                    //let mut new_node = BalancedBinarySearchNode::new(value, data);
                    //  new_node.parent = Some(Rc::downgrade(&node_ref));
                    self.right = Some(Box::new(BalancedBinarySearchNode::new(value, data)));
                    Ok(())
                }
            };
        } else {
            match self.left {
                Some(ref mut left) => BalancedBinarySearchNode::insert(left, value, data),
                None => {
                    //let mut new_node = BalancedBinarySearchNode::new(value, data);
                    //new_node.parent = Some(Rc::downgrade(&node_ref));
                    self.left = Some(Box::new(BalancedBinarySearchNode::new(value, data)));
                    Ok(())
                }
            };
        }
        Ok(())
    }

    pub fn is_leaf(&self) -> bool {
        if (self.left.is_some() | self.right.is_some()) {
            false
        } else {
            true
        }
    }

    fn rightmost_child(&mut self) -> Option<Box<BalancedBinarySearchNode<T>>> {
        match self.right {
            Some(ref mut right) => {
                if let Some(t) = right.rightmost_child() {
                    Some(t)
                } else {
                    let mut r = self.right.take();
                    if let Some(ref mut r) = r {
                        self.right = std::mem::replace(&mut r.left, None); // don't loose left node if some
                    }
                    r // return the node
                }
            }
            None => None,
        }
    }

    pub fn delete(
        mut this: Box<BalancedBinarySearchNode<T>>,
        target: &i32,
    ) -> Option<Box<BalancedBinarySearchNode<T>>> {
        if target < &this.key {
            if let Some(left) = this.left.take() {
                this.left = Self::delete(left, target);
            }
            return Some(this);
        }
        if target > &this.key {
            if let Some(right) = this.right.take() {
                this.right = Self::delete(right, target);
            }
            return Some(this);
        }
        assert!(target == &this.key, "Faulty ord");
        match (this.left.take(), this.right.take()) {
            (None, None) => None, // leaf just remove it
            (Some(left), None) => Some(left),
            (None, Some(right)) => Some(right),
            (Some(mut left), Some(right)) => {
                if let Some(mut rightmost) = left.rightmost_child() {
                    rightmost.left = Some(left);
                    rightmost.right = Some(right);
                    Some(rightmost)
                } else {
                    left.right = Some(right);
                    Some(left)
                }
            }
        }
    }

    pub fn imbalance(this: &Box<BalancedBinarySearchNode<T>>) -> i8 {
        let left_side_heigth = match &this.left {
            Some(node) => node.height(),
            _ => 0,
        };
        let right_side_height = match &this.right {
            Some(node) => node.height(),
            _ => 0,
        };
        (right_side_height - left_side_heigth) as i8
    }


    pub fn rebalance(mut this: Box<BalancedBinarySearchNode<T>>) -> Option<Box<BalancedBinarySearchNode<T>>> {

        let imbalance = BalancedBinarySearchNode::imbalance(&this);
        // println!("imbalance: {}", imbalance);

        if imbalance == 1 || imbalance == -1 {
            if let Some(right) = this.right.take() {
                this.right = Self::rebalance(right);
            }

            if let Some(right) = this.right.take() {
                this.right = Self::rebalance(right);
            }
            Some(this)

        } else if imbalance == 2 {
            if let Some(ref right) = this.right{
                let right_balance_factor = BalancedBinarySearchNode::imbalance(&right);
               // println!("left balance factor: {}", right_balance_factor);

                if right_balance_factor == 1 {
                    BalancedBinarySearchNode::rotate_left(this)
                } else if right_balance_factor == -1 {
                    BalancedBinarySearchNode::rotate_right_left(this)

                } else {
                    if let Some(right) = this.right.take() {
                        this.right = Self::rebalance(right);
                    }

                    if let Some(right) = this.right.take() {
                        this.right = Self::rebalance(right);
                    }
                    Some(this)
                }
            }
            else{
                panic!(format!("can't find left node"))
            }
        } else if imbalance == -2 {

            if let Some(ref left) = this.left {

                let left_balance_fator = BalancedBinarySearchNode::imbalance(&left);

                if left_balance_fator == -1 {
                    BalancedBinarySearchNode::rotate_right(this)

                } else if left_balance_fator == 1 {
                    BalancedBinarySearchNode::rotate_left_right(this)

                } else {
                    if let Some(right) = this.right.take() {
                        this.right = Self::rebalance(right);
                    }
                    if let Some(right) = this.right.take() {
                        this.right = Self::rebalance(right);
                    }
                    Some(this)
                }
            }
            else{
                            panic!("rebalance error")
            }
        }
        else{
            Some(this)
            //panic!("rebalance error")

        }
    }

    /// tree like:
    ///          Z
    ///        /   \
    ///       Y      4
    ///     /  \
    ///    x    3
    ///  /  \
    /// 1    2


    pub fn rotate_right_left (mut this: Box<BalancedBinarySearchNode<T>>) -> Option<Box<BalancedBinarySearchNode<T>>> {
        this.right = BalancedBinarySearchNode::rotate_right(this.right.take().unwrap());
        BalancedBinarySearchNode::rotate_left(this)
    }

    pub fn rotate_left_right (mut this: Box<BalancedBinarySearchNode<T>>) -> Option<Box<BalancedBinarySearchNode<T>>> {
        this.left = BalancedBinarySearchNode::rotate_left(this.left.take().unwrap());
        BalancedBinarySearchNode::rotate_right(this)
    }

    pub fn rotate_right(
        mut this: Box<BalancedBinarySearchNode<T>>,
    ) -> Option<Box<BalancedBinarySearchNode<T>>> {
        match this.left.take() {
            Some(mut left_child) => {
                this.left = left_child.right.take();
                left_child.right = Some(this);
                Some(left_child)
            }
            None => panic!(),
        }
    }

    pub fn rotate_left(
        mut this: Box<BalancedBinarySearchNode<T>>,
    ) -> Option<Box<BalancedBinarySearchNode<T>>> {
        match this.right.take() {
            Some(mut right_child) => {
                this.right = right_child.left.take();
                right_child.left = Some(this);
                Some(right_child)
            }
            None => panic!(),
        }
    }

    fn number_of_child(&self) -> u8 {
        if self.is_leaf() {
            0
        } else if (self.left.is_some() & self.right.is_some()) {
            2
        } else {
            1
        }
    }



    //
    //
    // }
}

pub struct BalancedBinarySearchTree<T> {
    root: Option<Box<BalancedBinarySearchNode<T>>>,
    count: u32,
}

impl<T> BalancedBinarySearchTree<T>
    where
        T: Clone + Copy,
{

    pub fn max(&self) -> Result<(i32, Option<T>), ErrorBBST>{
        match &self.root{
            Some(root) => Ok(root.deref().borrow().max()),
            None => Err(ErrorBBST::Unfound)
        }
    }

    // pub fn min(&self) -> Result<(i32, Option<Box<T>>), ErrorBBST>{
    //     match self.root{
    //         Some(root) => Ok(BalancedBinarySearchNode::min(root.deref().borrow())),
    //         None => Err(ErrorBBST::Unfound)
    //     }
    // }

    pub fn height(&self) -> Option<i32> {
        match &self.root {
            Some(node) => Some(node.deref().borrow_mut().height()),
            None => None,
        }
    }

    pub fn rebalance(&mut self) -> (){
        if let Some(root) =  self.root.take(){
            self.root = BalancedBinarySearchNode::rebalance(root);
        }
    }

    pub fn insert(&mut self, value: i32, data: Option<Box<T>>) -> Result<(), ErrorBBST> {
        match self.root {
            Some(ref mut root) =>{
                BalancedBinarySearchNode::insert(root, value, data);
                self.rebalance();
                self.count += 1;
            Ok(())},
            None => {
                self.root = Some(Box::new(BalancedBinarySearchNode::new(value, data)));
                self.count += 1;
                Ok(())
            }
        }
    }

    pub fn delete(&mut self, target: &i32) {
        if let Some(root) = self.root.take() {
            self.root = BalancedBinarySearchNode::delete(root, target);
            self.count -= 1;
        }
    }

    pub fn length(&self) -> u32{
        self.count
    }

    pub fn exists(&self, value: i32) -> bool {
        match &self.root {
            Some(node) => node.deref().borrow().exists(value),
            None => false,
        }
    }

    pub fn new() -> Self{
        BalancedBinarySearchTree{
            root: None,
            count: 0
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{BalancedBinarySearchNode, BalancedBinarySearchTree};
    use std::cell::RefCell;

    #[test]
    fn Insertion() {
        let mut my_tree = BalancedBinarySearchTree::<i32>::new();
        my_tree.insert(0, None);
        println!("{:?}", my_tree.height());
        my_tree.insert(4, None);
        println!("{:?}", my_tree.height());
        my_tree.insert(6, None);
        println!("{:?}", my_tree.height());
        my_tree.insert(-1, None);
        println!("{:?}", my_tree.height());
        my_tree.insert(-3, None);
        println!("{:?}", my_tree.exists(-3));
        my_tree.delete(&-3);
        println!("{:?}", my_tree.exists(-3));
    }
    fn balancing(){
        let mut my_tree = BalancedBinarySearchTree::<i32>::new();
        my_tree.insert(0, None);
        my_tree.insert(1, None);
        my_tree.insert(2, None);
        my_tree.insert(3, None);
        my_tree.insert(4, None);

    }

    fn max(){
        let mut my_tree = BalancedBinarySearchTree::<i32>::new();
        my_tree.insert(0, None);
        assert_eq!(my_tree.max().unwrap().0, 0);
        my_tree.insert(1, None);
        assert_eq!(my_tree.max().unwrap().0, 1);
        my_tree.insert(-1, None);
        assert_eq!(my_tree.max().unwrap().0, 1);
        my_tree.insert(3, None);
        assert_eq!(my_tree.max().unwrap().0, 3);
    }
    fn min(){
        let mut my_tree = BalancedBinarySearchTree::<i32>::new();
        my_tree.insert(0, None);
        assert_eq!(my_tree.max().unwrap().0, 0);
        my_tree.insert(1, None);
        assert_eq!(my_tree.max().unwrap().0, 0);
        my_tree.insert(-1, None);
        assert_eq!(my_tree.max().unwrap().0, -1);
    }
}
